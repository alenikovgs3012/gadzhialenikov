package com.example.calculate

import kotlin.math.pow

//Класс, считающий комплексные числа
class Complex (val first1: Float,val first2: Float, val second1: Float,val second2: Float) : Pair() {
//Тестовая функция
    override fun aboba(): Float {
        return first1-second1;
    }
//сложение
    override fun add(): String {
    if(first2 + second2<0) {
        return ("${first1 + second1}${first2 + second2}")
    }
    else {
        return ("${first1 + second1}+${first2 + second2}")
    }
    }
//вычитание
    override fun sub(): String {
    return ("${first1-second1}-${first2-second2}")
    }
//умножение
    override fun mult(): String {
    if (first1*second2+first2*second1 < 0){
        return ("${first1 * second1 - first2 * second2}${first1 * second2 + first2 * second1}")
    } else {
        return ("${first1 * second1 - first2 * second2}+${first1 * second2 + first2 * second1}")
    }
    }
//деление
    override fun divv(): String {
    if ((second1 * first2 - first1 * second2) / (second1.pow(2) + second2.pow(2)) < 0) {
        return ("${(first1 * second1 + first2 * second2) / (second1.pow(2) + second2.pow(2))}" +
                "${(second1 * first2 - first1 * second2) / (second1.pow(2) + second2.pow(2))}")
    }
    else {
        return ("${(first1 * second1 + first2 * second2) / ((second1).pow(2) + second2.pow(2))}" +
                "+${(second1 * first2 - first1 * second2) / (second1.pow(2) + second2.pow(2))}")
    }
    }

}