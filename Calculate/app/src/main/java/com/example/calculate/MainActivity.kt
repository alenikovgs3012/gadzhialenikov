package com.example.calculate

import android.os.Bundle
//import android.util.Half.toFloat
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import com.example.calculate.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    lateinit var myButton: Button
    lateinit var myEdit: EditText
    lateinit var MySwitch: Switch
    lateinit var Calc: Pair

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        myEdit = findViewById(R.id.editText)//объект myEdit связываем с элементом интерфейса editText1
        MySwitch = findViewById(R.id.switch1)

        MySwitch.setOnClickListener {
            if(MySwitch.isChecked)
            {MySwitch.text="Интерв"}
            else {MySwitch.text="Комп"}
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    //Функция "Нажал на кнопку, вывело в строку"
    fun numBtnPress(view: View) {
        val btn = view as Button
        val num = btn.text.toString()
        myEdit.setText(myEdit.text.toString() + num)
    }

    //Функция для операций
    fun MyOperation(view: View) {
        val btn = view as Button
        val num = btn.text.toString()
        if (MySwitch.isChecked) { //Когда свич переключен на интервалнамбер
            try {//Пытаемся собрать массив и вывести ответ
                var MySplitString = StringMassiveBuilder(myEdit.text.toString()).BuildInterval()//Вызов функции, которая и соберёт массив
                Calc = IntervalNumber (MySplitString[0].toFloat(),MySplitString[1].toFloat(),MySplitString[3].toFloat(),MySplitString[4].toFloat())//Вызываем конструктор
                when (MySplitString[2]) {//Выбор вызываемой функции.
                    "-" -> myEdit.setText(Calc.sub()+num)
                    "+" -> myEdit.setText(Calc.add()+num)
                    "*" -> myEdit.setText(Calc.mult()+num)
                    "/" -> myEdit.setText(Calc.divv()+num)
                }
            } catch (e: Exception) {//Если ничего не получается - просто добавляем символ
                myEdit.setText(myEdit.text.toString() + num)
            }
        } else {//Комплексное число
            try {//Пытаемся собрать массив и вывести ответ
                var MySplitString = StringMassiveBuilder(myEdit.text.toString()).BuildComplex()//Вызов функции, которая и соберёт массив
                Calc = Complex (MySplitString[0].toFloat(),MySplitString[1].toFloat(),MySplitString[3].toFloat(),MySplitString[4].toFloat())//Вызываем конструктор
                when (MySplitString[2]) {//Выбор вызываемой функции.
                    "-" -> myEdit.setText(Calc.sub()+num)
                    "+" -> myEdit.setText(Calc.add()+num)
                    "*" -> myEdit.setText(Calc.mult()+num)
                    "/" -> myEdit.setText(Calc.divv()+num)
                }
            } catch(e: Exception) {//Если ничего не получается - просто добавляем символ
                myEdit.setText(myEdit.text.toString() + num)
            }
        }
    }

    //Функция "удалить всё"
    fun allDrop(view: View) {
        myEdit.setText("")
    }

    //Функция "Удалить одно"
    fun oneDrop(view: View) {
        myEdit.setText(myEdit.text.toString().replaceFirst(".$".toRegex(), ""))

    }

    //функция "равно"
    fun equals(view: View) {
        if (MySwitch.isChecked) { //Когда свич переключен на интервалнамбер
            try {//Пытаемся собрать массив и вывести ответ
                //myEdit.setText(StringMassiveBuilder(myEdit.text.toString(), Calc, num).BuildInterval())

                var MySplitString = StringMassiveBuilder(myEdit.text.toString()).BuildInterval()
                //myEdit.setText(MySplitString[0]+" "+MySplitString[1]+" "+MySplitString[2]+" "+MySplitString[3]+" "+MySplitString[4])
                Calc = IntervalNumber (MySplitString[0].toFloat(),MySplitString[1].toFloat(),MySplitString[3].toFloat(),MySplitString[4].toFloat())//Вызываем конструктор
                when (MySplitString[2]) {//Выбор вызываемой функции.
                    "-" -> myEdit.setText(Calc.sub())
                    "+" -> myEdit.setText(Calc.add())
                    "*" -> myEdit.setText(Calc.mult())
                    "/" -> myEdit.setText(Calc.divv())
                }
            } catch (e: Exception) {//Если ничего не получается - просто добавляем символ
            }
        } else {//Комплексное число
            try {//Пытаемся собрать массив и вывести ответ
                var MySplitString = StringMassiveBuilder(myEdit.text.toString()).BuildComplex()
                //myEdit.setText(MySplitString[0]+" "+MySplitString[1]+" "+MySplitString[2]+" "+MySplitString[3]+" "+MySplitString[4])
                Calc = Complex (MySplitString[0].toFloat(),MySplitString[1].toFloat(),MySplitString[3].toFloat(),MySplitString[4].toFloat())//Вызываем конструктор
                when (MySplitString[2]) {//Выбор вызываемой функции.
                    "-" -> myEdit.setText(Calc.sub())
                    "+" -> myEdit.setText(Calc.add())
                    "*" -> myEdit.setText(Calc.mult())
                    "/" -> myEdit.setText(Calc.divv())
                }
            } catch(e: Exception) {//Если ничего не получается - ничего не делаем
            }
        }
    }


}