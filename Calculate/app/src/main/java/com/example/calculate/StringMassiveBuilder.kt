package com.example.calculate

class StringMassiveBuilder(var MyString: String) {

    fun BuildInterval():Array<String>{
        var mySplitString = emptyArray<String>()//Создаю массив строк
        mySplitString += MyString.substringBefore(',')//Вбиваю в массив первую цифру
        MyString=MyString.substringAfter(',')//Вырезаю из строки первую цифру
        if (MyString[0] == '-') {//Если второе число отрицательное
            MyString=MyString.substringAfter('-')//Вырезаю минус из строки
            mySplitString += MyString.substringBefore('+').substringBefore('/').substringBefore('*').substringBefore('-')//Вбиваю в массив вторую цифру
            MyString = MyString.substring(mySplitString[1].length)//Вырезаем вторую цифру из строки
            mySplitString[1]="-"+mySplitString[1]//И возвращаю минус во второй элемент массива
        }
        else {//Если второе число положительное
            mySplitString += MyString.substringBefore('+').substringBefore('/').substringBefore('*').substringBefore('-')//Вбиваю в массив вторую цифру
            MyString = MyString.substring(mySplitString[1].length)//Вырезаем вторую цифру из строки
        }
        mySplitString += MyString[0].toString()//Вбиваю в массив символ операции
        MyString = MyString.substring(1)//Вырезаю из строки символ операции

        mySplitString += MyString.substringBefore(',')//Вбиваю в массив третье число
        mySplitString += MyString.substringAfter(',')//Вбиваю в массив четвёртое число
        return(mySplitString)
    }

    fun BuildComplex():Array<String> {
        var mySplitString = emptyArray<String>()//Создаю массив строк
        for (i in 0..4) {//в цикле разбираем строку
            if (i == 2) {//Если это этап взятия символа операции
                mySplitString += MyString[0].toString()//Вбиваю в массив символ операции
                MyString = MyString.substring(1)//Вырезаю из строки символ операции
            } else {//Если это число
                if (MyString[0] == '-') {//Если число отрицательное
                    MyString = MyString.substringAfter('-')//Вырезаю минус из строки
                    mySplitString += MyString.substringBefore('+').substringBefore('/')
                        .substringBefore('*').substringBefore('-')//Вбиваю в массив число
                    MyString =
                        MyString.substring(mySplitString[i].length)//Вырезаем число из строки
                    mySplitString[i] =
                        "-" + mySplitString[i]//И возвращаю минус в элемент массива
                } else {//Если число положительное
                    if (MyString[0] == '+') {
                        MyString = MyString.substringAfter('+')//Вырезаю плюс из строки
                        mySplitString += MyString.substringBefore('+').substringBefore('/')
                            .substringBefore('*')
                            .substringBefore('-')//Вбиваю в массив число
                        MyString =
                            MyString.substring(mySplitString[i].length)//Вырезаем число из строки
                    }
                    else {
                        mySplitString += MyString.substringBefore('+').substringBefore('/')
                            .substringBefore('*')
                            .substringBefore('-')//Вбиваю в массив число
                        MyString =
                            MyString.substring(mySplitString[i].length)//Вырезаем число из строки
                    }
                }
            }
        }
        return(mySplitString)

    }
}