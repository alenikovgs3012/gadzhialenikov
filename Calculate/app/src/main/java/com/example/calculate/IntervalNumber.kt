package com.example.calculate

import java.lang.Float.min
import java.lang.Float.max

//Класс, считающий интервальные числа
class IntervalNumber(val first1: Float,val first2: Float, val second1: Float,val second2: Float) : Pair() {
    override fun aboba(): Float {//Тестовая функция
        return first1+second2;
    }

    override fun add(): String {
        return ("${first1+second1},${first2+second2}")
    }

    override fun sub(): String {
        return ("${first1-second2},${second1-first2}")
    }
    //first1 a first2 b second1 c second2 d
    override fun mult(): String {
        return ("${
            min(
                min((first1*second1),(first1*second2)),min((first2*second1),(first2*second2))
            )}" +
                ",${max(
                    max((first1*second1),(first1*second2)),max((first2*second1),(first2*second2))
                )}")
    }

    override fun divv(): String {
        return ("${
            min(
                min((first1/second1),(first1/second2)),min((first2/second1),(first2/second2))
            )}" +
                ",${max(
                    max((first1/second1),(first1/second2)),max((first2/second1),(first2/second2))
                )}")
    }

}